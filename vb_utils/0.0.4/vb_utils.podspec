Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '9.0'
s.name = "vb_utils"
s.summary = "Vidumanszki ios utils"
s.requires_arc = true

# 2
s.version = "0.0.4"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Balazs Vidumanszki" => "balazs@vidumanszki.hu" }

# 5 - Replace this URL with your own Github page's URL (from the address bar)
s.homepage = "https://bitbucket.org/vbalazs137/ios-utils"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://vbalazs137@bitbucket.org/vbalazs137/ios-utils.git", :tag => "#{s.version}"}

# 7
s.framework = "UIKit"

# 8
s.source_files = "utils/**/*.{swift}"

# 9
#s.resources = "RWPickFlavor/**/*.{png,jpeg,jpg,storyboard,xib}"

end
